package com.trumpetx.minerstatus.tasks;

import android.database.Cursor;
import android.os.AsyncTask;

import com.trumpetx.minerstatus.beans.StatusMetadata;
import com.trumpetx.minerstatus.beans.TickerMetadata;
import com.trumpetx.minerstatus.service.MinerService;
import com.trumpetx.minerstatus.util.Configuration;
import com.trumpetx.minerstatus.util.DataAgent;
import com.trumpetx.minerstatus.util.Metadata;

public abstract class AsynchMinerUpdateTask
    extends AsyncTask<Object, Integer, Boolean>
{
    private MinerService _minerService;

    private Configuration _configuration;

    private String[] _apiKeysToUpdate;

    private DataAgent _dataAgent;

    protected AsynchMinerUpdateTask( MinerService minerService, Configuration configuration, String... apiKeysToUpdate )
    {
        _minerService = minerService;
        _configuration = configuration;
        _apiKeysToUpdate = apiKeysToUpdate;

        if ( _apiKeysToUpdate.length == 0 )
            _apiKeysToUpdate = null;

        _dataAgent = new DataAgent( configuration );
    }

    protected abstract void onPostExecute( Boolean result );

    protected Boolean doInBackground( Object... params )
    {
        // If we've specified specific miners to update, just update those
        if ( _apiKeysToUpdate != null )
        {
            for ( String apiKey : _apiKeysToUpdate )
                saveMinerData( apiKey, _minerService.getPoolForMiner( apiKey ) );
        }
        else
        {
            for ( TickerMetadata tickerMetadata : Metadata.getTickerMetadatas() )
            {
                String key = tickerMetadata.getName();

                if ( _configuration.getShowExchange( key ) )
                {
                    String data = tickerMetadata.getDataAdapter().fetchData( _dataAgent );

                    _minerService.addJsonData( key, data, 0 );
                }
            }

            Cursor poolCursor = _minerService.getPools();

            while ( poolCursor.moveToNext() )
            {
                String pool = poolCursor.getString( 0 );

                Cursor cursor = _minerService.getMiners( poolCursor.getString( 0 ) );

                while ( cursor.moveToNext() )
                {
                    String apiKey = cursor.getString( 0 );
                    saveMinerData( apiKey, pool );
                }

                if ( cursor != null && !cursor.isClosed() )
                    cursor.close();
            }

            if ( poolCursor != null && !poolCursor.isClosed() )
                poolCursor.close();

            _configuration.setLastUpdated( System.currentTimeMillis() );
        }

        return Boolean.TRUE;
    }

    private void saveMinerData( String apiKey, String pool )
    {
        if ( pool.equals( "" ) )
            throw new RuntimeException( "Can't find pool value from DB\nPool: " + pool + "\nApiKey: " + apiKey );

        StatusMetadata poolMetadata = Metadata.getPoolMetadata( pool );

        String[] data = poolMetadata.getDataAdapter().fetchData( apiKey, _dataAgent );

        for ( int dataIndex = 0; dataIndex < data.length; dataIndex++ )
            _minerService.addJsonData( apiKey, data[dataIndex], dataIndex );
    }
}
