package com.trumpetx.minerstatus;

import android.app.Activity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.KeyEvent;
import com.trumpetx.minerstatus.service.MinerService;
import com.trumpetx.minerstatus.service.ThemeService;
import com.trumpetx.minerstatus.util.Configuration;

public abstract class AbstractMinerStatusActivity
    extends Activity
{
    protected MinerService _minerService;

    protected ThemeService _themeService;

    protected Configuration _configuration;

    @Override
    protected void onResume()
    {
        super.onResume();
    }

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

        MinerStatusApp application = (MinerStatusApp) getApplication().getApplicationContext();

        initializeServices( application );
    }

    protected void initializeServices( MinerStatusApp application )
    {
        _minerService = application.getMinerService();
        _themeService = application.getThemeService();

        _configuration = new Configuration( application );
    }

    protected int getDip( float dipValue )
    {
        return (int) TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP, (float) dipValue,
                                                getResources().getDisplayMetrics() );
    }

    @Override
    public boolean onKeyDown( int keyCode, KeyEvent event )
    {
        if ( ( keyCode == KeyEvent.KEYCODE_BACK ) )
        {
            return true;
        }
        else
        {
            return super.onKeyDown( keyCode, event );
        }
    }

    @Override
    public boolean onKeyUp( int keyCode, KeyEvent event )
    {
        if ( ( keyCode == KeyEvent.KEYCODE_BACK ) )
        {
            finish();
        }
        return super.onKeyUp( keyCode, event );
    }
}
