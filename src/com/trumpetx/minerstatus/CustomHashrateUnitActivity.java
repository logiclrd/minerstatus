package com.trumpetx.minerstatus;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import com.trumpetx.minerstatus.hashrateunits.HashrateUnit;

public class CustomHashrateUnitActivity
    extends AbstractMinerStatusActivity
{
    private int color;

    private int bgColor;

    public CustomHashrateUnitActivity()
    {
    }

    private CustomHashrateUnitActivity( Context applicationContext, PopupWindow popupWindow )
    {
        _popupWindow = popupWindow;
        _layout = popupWindow.getContentView();
        _applicationContext = applicationContext;
    }

    Context _applicationContext;

    PopupWindow _popupWindow;

    View _layout;

    OnHashrateUnitChanged _newValueHandler;

    OnPopupDismissed _popupDismissedHandler;

    public void setNewValueHandler( OnHashrateUnitChanged newValueHandler )
    {
        _newValueHandler = newValueHandler;
    }

    public void setPopupDismissedHandler( OnPopupDismissed popupDismissedHandler )
    {
        _popupDismissedHandler = popupDismissedHandler;
    }

    @Override
    public Context getApplicationContext()
    {
        if ( _applicationContext != null )
        {
            return _applicationContext;
        }
        else
        {
            return super.getApplicationContext();
        }
    }

    @Override
    public View findViewById( int id )
    {
        if ( _layout != null )
        {
            return _layout.findViewById( id );
        }
        else
        {
            return super.findViewById( id );
        }
    }

    LinearLayout customHashrateUnitView;

    TextView headingLabel;

    TextView nameLabel;

    EditText nameEdit;

    TextView scaleLabel;

    EditText scaleEdit;

    Button okButton;

    Button cancelButton;

    private void findViews()
    {
        customHashrateUnitView = (LinearLayout) findViewById( R.id.customHashrateUnitView );
        headingLabel = (TextView) findViewById( R.id.headingLabel );
        nameLabel = (TextView) findViewById( R.id.nameLabel );
        nameEdit = (EditText) findViewById( R.id.nameEdit );
        scaleLabel = (TextView) findViewById( R.id.scaleLabel );
        scaleEdit = (EditText) findViewById( R.id.scaleEdit );
        okButton = (Button) findViewById( R.id.okButton );
        cancelButton = (Button) findViewById( R.id.cancelButton );
    }

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

        setContentView( R.layout.custom_hashrate_unit );

        onCreateDirect();
    }

    private void onCreateDirect()
    {
        findViews();

        bgColor = _themeService.getTheme().getBackgroundColor();
        color = _themeService.getTheme().getTextColor();

        customHashrateUnitView.setBackgroundColor( bgColor );

        headingLabel.setTextColor( color );
        nameLabel.setTextColor( color );
        scaleLabel.setTextColor( color );

        if ( _hashrateUnit != null )
        {
            nameEdit.setText( _hashrateUnit.getName() );
            scaleEdit.setText( String.valueOf( _hashrateUnit.getScale() ) );
        }

        okButton.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                acceptPopup();
            }
        } );

        cancelButton.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                cancelPopup();
            }
        } );
    }

    HashrateUnit _hashrateUnit;

    public HashrateUnit getHashrateUnit()
    {
        return _hashrateUnit;
    }

    public void setHashrateUnit( HashrateUnit copyFrom )
    {
        _hashrateUnit = copyFrom.clone();
    }

    private void acceptPopup()
    {
        if ( saveData() )
        {
            closePopup();
        }
    }

    private void cancelPopup()
    {
        closePopup();
    }

    boolean _allowEmptyName = false;

    private boolean saveData()
    {
        if ( _hashrateUnit == null )
        {
            _hashrateUnit = new HashrateUnit();
        }

        String nameValue;
        double scaleValue;

        nameValue = nameEdit.getText().toString().trim();

        if ( ( nameValue.length() == 0 ) && !_allowEmptyName )
        {
            _allowEmptyName = true;

            Toast.makeText( getApplicationContext(), "Press OK again to save with a blank name",
                            Toast.LENGTH_LONG ).show();

            return false;
        }

        try
        {
            scaleValue = Double.valueOf( scaleEdit.getText().toString() );
        }
        catch ( Exception e )
        {
            Toast.makeText( getApplicationContext(), "Invalid format for scale - try 1e6 or 1000000",
                            Toast.LENGTH_LONG ).show();

            return false;
        }

        _hashrateUnit.setName( nameValue );
        _hashrateUnit.setScale( scaleValue );
        _hashrateUnit.setIsAuto( false );
        _hashrateUnit.setIsAutoForEach( false );

        if ( _newValueHandler != null )
        {
            _newValueHandler.onHashrateUnitChanged( _hashrateUnit );
        }

        return true;
    }

    private void closePopup()
    {
        if ( _popupWindow != null )
        {
            _popupWindow.dismiss();
        }

        if ( _popupDismissedHandler != null )
        {
            _popupDismissedHandler.onPopupDismissed();
        }
    }

    public static void showAsPopup( Activity owner, View parent, HashrateUnit initialValue,
                                    OnHashrateUnitChanged newValueHandler, OnPopupDismissed popupDismissedHandler )
    {
        LayoutInflater inflater = (LayoutInflater) owner.getSystemService( Context.LAYOUT_INFLATER_SERVICE );

        View layout = inflater.inflate( R.layout.custom_hashrate_unit, null );

        PopupWindow popupWindow = new PopupWindow( layout );

        CustomHashrateUnitActivity activity =
            new CustomHashrateUnitActivity( owner.getApplicationContext(), popupWindow );

        activity.setHashrateUnit( initialValue );
        activity.setNewValueHandler( newValueHandler );
        activity.setPopupDismissedHandler( popupDismissedHandler );
        if ( owner.getApplication().getApplicationContext() instanceof MinerStatusApp )
        {
            activity.initializeServices( (MinerStatusApp) owner.getApplication().getApplicationContext() );
        }
        activity.onCreateDirect();

        popupWindow.setFocusable( true );
        popupWindow.setWindowLayoutMode( LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT );
        popupWindow.setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN );
        popupWindow.showAtLocation( parent, Gravity.CENTER, 0, -30 );
    }

    public interface OnHashrateUnitChanged
    {
        void onHashrateUnitChanged( HashrateUnit newHashrateUnit );
    }

    public interface OnPopupDismissed
    {
        void onPopupDismissed();
    }
}
