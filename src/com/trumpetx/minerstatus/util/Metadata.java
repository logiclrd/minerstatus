package com.trumpetx.minerstatus.util;

import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.util.Log;

import com.trumpetx.minerstatus.beans.Plugin;
import com.trumpetx.minerstatus.beans.PluginMetadata;
import com.trumpetx.minerstatus.beans.Status;
import com.trumpetx.minerstatus.beans.StatusMetadata;
import com.trumpetx.minerstatus.beans.Ticker;
import com.trumpetx.minerstatus.beans.TickerMetadata;

import dalvik.system.DexFile;
import dalvik.system.PathClassLoader;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Metadata
{
    final static String TAG = "Metadata";

    public static int MAX_ERRORS = 10;

    public static int CONNECTION_TIMEOUT = 4000;

    public static int SOCKET_TIMEOUT = 0;

    public static boolean HASHRATE_COMMAS = false;

    public static int HASHRATE_DECIMALS = 2;

    static final Map<String, TickerMetadata> s_tickerMetadata = new HashMap<String, TickerMetadata>( 5 );

    static final Map<String, StatusMetadata> s_poolMetadata = new HashMap<String, StatusMetadata>( 20 );

    static final Map<String, String> s_themeList = new HashMap<String, String>( 2 );

    public static Set<String> getTickerNames()
    {
        return s_tickerMetadata.keySet();
    }

    public static TickerMetadata getTickerMetadata( String tickerName )
    {
        return s_tickerMetadata.get( tickerName );
    }

    public static Iterable<TickerMetadata> getTickerMetadatas()
    {
        return s_tickerMetadata.values();
    }

    public static Set<String> getPoolNames()
    {
        return s_poolMetadata.keySet();
    }

    public static StatusMetadata getPoolMetadata( String poolName )
    {
        return s_poolMetadata.get( poolName );
    }

    public static Iterable<StatusMetadata> getPoolMetadatas()
    {
        return s_poolMetadata.values();
    }

    static Application getCurrentApplication()
        throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException
    {
        // From:
        // http://stackoverflow.com/questions/2002288/static-way-to-get-context-on-android=

        final Class<?> activityThreadClass = Class.forName( "android.app.ActivityThread" );
        final Method currentApplicationMethod = activityThreadClass.getMethod( "currentApplication" );

        return (Application) currentApplicationMethod.invoke( null, (Object[]) null );
    }

    static void findClassesImplementingInterface( String rootPackageName, Class<?> interfaceType,
                                                  List<Class<?>> implementors )
    {
        // From:
        // http://stackoverflow.com/questions/5105842/java-custom-annotation-and-dynamic-loading

        Context currentApplication;
        ApplicationInfo applicationInfo;
        DexFile dexFile;
        ClassLoader classLoader;

        try
        {
            currentApplication = getCurrentApplication();
            applicationInfo = currentApplication.getPackageManager().getApplicationInfo( rootPackageName, 0 );

            String apkName = applicationInfo.sourceDir;

            dexFile = new DexFile( apkName );
            classLoader = new PathClassLoader( apkName, Thread.currentThread().getContextClassLoader() );
        }
        catch ( Throwable t )
        {
            return;
        }

        Enumeration<String> dexFileEntries = dexFile.entries();

        String packageNamePrefix = rootPackageName + ".";

        while ( dexFileEntries.hasMoreElements() )
        {
            String dexFileEntry = dexFileEntries.nextElement();

            if ( !dexFileEntry.startsWith( packageNamePrefix ) )
                continue;

            try
            {
                Class<?> type = classLoader.loadClass( dexFileEntry );

                if ( interfaceType.isAssignableFrom( type ) )
                    implementors.add( type );
            }
            catch ( Throwable t )
            {
            }
        }
    }

    static
    {
        List<Class<?>> allPluginTypes = new ArrayList<Class<?>>();

        findClassesImplementingInterface( "com.trumpetx.minerstatus", Plugin.class, allPluginTypes );

        for ( Class<?> type : allPluginTypes )
        {
            if ( Modifier.isInterface( type.getModifiers() ) || Modifier.isAbstract( type.getModifiers() ) )
                continue;

            @SuppressWarnings( "unchecked" )
            Class<? extends Plugin> pluginType = (Class<? extends Plugin>) type;

            try
            {
                Constructor<? extends Plugin> ctor = pluginType.getDeclaredConstructor();

                ctor.setAccessible( true );

                PluginMetadata pluginMetadata = ctor.newInstance().getMetadata();

                pluginMetadata.getDeserializer().performStartupInitialization();

                if ( pluginMetadata instanceof TickerMetadata )
                    s_tickerMetadata.put( pluginMetadata.getName(), (TickerMetadata) pluginMetadata );

                if ( pluginMetadata instanceof StatusMetadata )
                    s_poolMetadata.put( pluginMetadata.getName(), (StatusMetadata) pluginMetadata );
            }
            catch ( Throwable t )
            {
                String objectType = "plugin";

                if ( Ticker.class.isAssignableFrom( type ) )
                    objectType = "ticker";
                else if ( Status.class.isAssignableFrom( type ) )
                    objectType = "pool status object";

                Log.d( TAG, "Unable to instantiate " + objectType + " from type '" + pluginType + "': " + t );
            }
        }
    }

    static
    {
        s_themeList.put( "dark", "Dark Theme" );
        s_themeList.put( "light", "Light Theme" );
    }
}
