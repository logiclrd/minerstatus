package com.trumpetx.minerstatus.util;

public interface Deserializer<T>
{
    void performStartupInitialization();

    T deserialize( String data );
}
