package com.trumpetx.minerstatus.util;

import com.trumpetx.minerstatus.MinerStatusApp;
import com.trumpetx.minerstatus.hashrateunits.HashrateUnit;
import com.trumpetx.minerstatus.service.ConfigService;

import java.math.BigDecimal;
import java.util.NoSuchElementException;

public class Configuration
{
    private ConfigService _configService;

    public Configuration( MinerStatusApp application )
    {
        _configService = application.getConfigService();
    }

    public static final String EXCHANGE_MTGOX = "mtgox";

    public static final String POOL_BITCOINPOOL = "bitcoinpool";

    public static final String POOL_SLUSH = "slush";

    public static final String POOL_DEEPBIT = "deepbit";

    public static final String POOL_BTCMINE = "btcmine";

    public static final String POOL_BTCGUILD = "btcguild";

    public static final String POOL_BITCLOCKERS = "bitclockers";

    public static final String POOL_OZCOIN = "ozcoin";

    public static final String POOL_ELIGIUS = "eligius";

    public static final String POOL_ECLIPSEMC = "eclipsemc";

    public static final String POOL_ABC = "abc";

    public static final String POOL_SIMPLECOIN = "simplecoin";

    public static final String POOL_TRIPLE = "triple";

    public static final String POOL_HHTT = "hhtt";

    public static final String POOL_BITMINTER = "bitminter";

    static final String KEY_SHOW_EXCHANGE_MTGOX = "show." + EXCHANGE_MTGOX;

    static final String KEY_SHOW_ADVERTISEMENTS = "show.ads";

    static final String KEY_CONNECTION_TIMEOUT = "connection.timeout";

    static final String KEY_MAXIMUM_ERRORS = "max.errors";

    static final String KEY_THEME = "theme";

    static final String KEY_WIDGET_APIKEY = "widget.apiKey";

    static final String KEY_HASHRATE_FORMAT_COMMAS = "hashrate.format.commas";

    static final String KEY_HASHRATE_FORMAT_DECIMALS = "hashrate.format.decimals";

    static final String KEY_HASHRATE_UNIT_SCALE = "hashrate.unit.scale";

    static final String KEY_HASHRATE_UNIT_NAME = "hashrate.unit.name";

    static final String KEY_HASHRATE_UNIT_ISAUTO = "hashrate.unit.isauto";

    static final String KEY_HASHRATE_UNIT_ISAUTOFOREACH = "hashrate.unit.isautoforeach";

    static final String KEY_LOW_HASHRATE_NOTIFICATION = "low.hashrate.notification";

    static final String KEY_VIBRATE_ON_NOTIFICATION = "vibrate.on.notification";

    static final String KEY_LAST_UPDATED = "last.updated";

    static final String VALUE_LOW_HASHRATE_NOTIFICATION_OFF = "off";

    static final String VALUE_LAST_UPDATED_NEVER = "never";

    public boolean getShowAdvertisements()
    {
        try
        {
            return Boolean.valueOf( _configService.getConfigValue( KEY_SHOW_ADVERTISEMENTS ) );
        }
        catch ( Throwable t )
        {
            return true;
        }
    }

    public void setShowAdvertisements( boolean newValue )
    {
        _configService.setConfigValue( KEY_SHOW_ADVERTISEMENTS, newValue ? "true" : "false" );
    }

    public boolean getShowExchange( String exchangeKey )
        throws NoSuchElementException
    {
        if ( exchangeKey.equals( EXCHANGE_MTGOX ) )
        {
            return getShowExchangeMtGox();
        }
        else
        {
            throw new NoSuchElementException( "Internal error: Invalid exchange key: " + exchangeKey );
        }
    }

    public void setShowExchange( String exchangeKey, boolean newValue )
        throws NoSuchElementException
    {
        if ( exchangeKey.equals( EXCHANGE_MTGOX ) )
        {
            setShowExchangeMtGox( newValue );
        }
        else
        {
            throw new NoSuchElementException( "Internal error: Invalid exchange key: " + exchangeKey );
        }
    }

    public boolean getShowExchangeMtGox()
    {
        try
        {
            return Boolean.valueOf( _configService.getConfigValue( KEY_SHOW_EXCHANGE_MTGOX ) );
        }
        catch ( Throwable t )
        {
            return false;
        }
    }

    public void setShowExchangeMtGox( boolean newValue )
    {
        _configService.setConfigValue( KEY_SHOW_EXCHANGE_MTGOX, String.valueOf( newValue ) );
    }

    public int getConnectionTimeoutMilliseconds()
    {
        try
        {
            return Integer.parseInt( _configService.getConfigValue( KEY_CONNECTION_TIMEOUT ) );
        }
        catch ( Throwable t )
        {
            return Metadata.CONNECTION_TIMEOUT;
        }
    }

    public void setConnectionTimeoutMilliseconds( int newMilliseconds )
    {
        _configService.setConfigValue( KEY_CONNECTION_TIMEOUT, String.valueOf( newMilliseconds ) );
    }

    public int getConnectionTimeout()
    {
        return getConnectionTimeoutMilliseconds() / 1000;
    }

    public void setConnectionTimeout( int newSeconds )
    {
        setConnectionTimeoutMilliseconds( newSeconds * 1000 );
    }

    public int getMaximumErrors()
    {
        try
        {
            return Integer.parseInt( _configService.getConfigValue( KEY_MAXIMUM_ERRORS ) );
        }
        catch ( Throwable t )
        {
            return Metadata.MAX_ERRORS;
        }
    }

    public void setMaximumErrors( int newValue )
    {
        _configService.setConfigValue( KEY_MAXIMUM_ERRORS, String.valueOf( newValue ) );
    }

    public String getTheme()
    {
        return _configService.getConfigValue( KEY_THEME );
    }

    public void setTheme( String newThemeName )
    {
        _configService.setConfigValue( KEY_THEME, newThemeName );
    }

    public String getWidgetApiKey()
    {
        String widgetApiKey = _configService.getConfigValue( KEY_WIDGET_APIKEY );

        if ( widgetApiKey.equals( "none" ) )
        {
            widgetApiKey = "";
        }

        return widgetApiKey;
    }

    public void setWidgetApiKey( String newValue )
    {
        _configService.setConfigValue( KEY_WIDGET_APIKEY, newValue );
    }

    public boolean getHashrateFormatCommas()
    {
        try
        {
            return Boolean.valueOf( _configService.getConfigValue( KEY_HASHRATE_FORMAT_COMMAS ) );
        }
        catch ( Throwable t )
        {
            return Metadata.HASHRATE_COMMAS;
        }
    }

    public void setHashrateFormatCommas( boolean newValue )
    {
        _configService.setConfigValue( KEY_HASHRATE_FORMAT_COMMAS, newValue ? "true" : "false" );
    }

    public int getHashrateFormatDecimals()
    {
        try
        {
            return Integer.parseInt( _configService.getConfigValue( KEY_HASHRATE_FORMAT_DECIMALS ) );
        }
        catch ( Throwable t )
        {
            return Metadata.HASHRATE_DECIMALS;
        }
    }

    public void setHashrateFormatDecimals( int newValue )
    {
        _configService.setConfigValue( KEY_HASHRATE_FORMAT_DECIMALS, String.valueOf( newValue ) );
    }

    public HashrateUnit getHashrateUnit()
    {
        HashrateUnit ret = new HashrateUnit();

        try
        {
            ret.setScale( Double.parseDouble( _configService.getConfigValue( KEY_HASHRATE_UNIT_SCALE ) ) );
            ret.setName( _configService.getConfigValue( KEY_HASHRATE_UNIT_NAME ) );
            ret.setIsAuto( Boolean.valueOf( _configService.getConfigValue( KEY_HASHRATE_UNIT_ISAUTO ) ) );
            ret.setIsAutoForEach( Boolean.valueOf( _configService.getConfigValue( KEY_HASHRATE_UNIT_ISAUTOFOREACH ) ) );
        }
        catch ( Throwable t )
        {
            ret.setScale( 1 );
            ret.setName( "H/s" );
            ret.setIsAuto( false );
            ret.setIsAutoForEach( false );
        }

        return ret;
    }

    public void setHashrateUnit( HashrateUnit newValue )
    {
        _configService.setConfigValue( KEY_HASHRATE_UNIT_SCALE, String.valueOf( newValue.getScale() ) );
        _configService.setConfigValue( KEY_HASHRATE_UNIT_NAME, newValue.getName() );
        _configService.setConfigValue( KEY_HASHRATE_UNIT_ISAUTO, newValue.getIsAuto() ? "true" : "false" );
        _configService.setConfigValue( KEY_HASHRATE_UNIT_ISAUTOFOREACH,
                                       newValue.getIsAutoForEach() ? "true" : "false" );
    }

    public BigDecimal getLowHashrateNotification()
    {
        try
        {
            String serializedValue = _configService.getConfigValue( KEY_LOW_HASHRATE_NOTIFICATION );

            if ( serializedValue != VALUE_LOW_HASHRATE_NOTIFICATION_OFF )
            {
                return new BigDecimal( serializedValue );
            }
        }
        catch ( Throwable t )
        {
        }

        return null;
    }

    public void setLowHashrateNotification( BigDecimal newValue )
    {
        String serializedValue;

        if ( newValue == null )
        {
            serializedValue = VALUE_LOW_HASHRATE_NOTIFICATION_OFF;
        }
        else
        {
            serializedValue = String.valueOf( newValue );
        }

        _configService.setConfigValue( KEY_LOW_HASHRATE_NOTIFICATION, serializedValue );
    }

    public boolean getVibrateOnNotification()
    {
        try
        {
            return Boolean.valueOf( _configService.getConfigValue( KEY_VIBRATE_ON_NOTIFICATION ) );
        }
        catch ( Throwable t )
        {
            return false;
        }
    }

    public void setVibrateOnNotification( boolean newValue )
    {
        _configService.setConfigValue( KEY_VIBRATE_ON_NOTIFICATION, newValue ? "true" : "false" );
    }

    public Long getLastUpdated()
    {
        try
        {
            String serializedValue = _configService.getConfigValue( KEY_LAST_UPDATED );

            if ( serializedValue != VALUE_LAST_UPDATED_NEVER )
            {
                return Long.parseLong( serializedValue );
            }
        }
        catch ( Throwable t )
        {
        }

        return null;
    }

    public void setLastUpdated( Long newValue )
    {
        String serializedValue;

        if ( newValue == null )
        {
            serializedValue = VALUE_LAST_UPDATED_NEVER;
        }
        else
        {
            serializedValue = String.valueOf( newValue );
        }

        _configService.setConfigValue( KEY_LAST_UPDATED, serializedValue );
    }
}
