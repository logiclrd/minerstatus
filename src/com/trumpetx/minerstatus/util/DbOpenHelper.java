package com.trumpetx.minerstatus.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.trumpetx.minerstatus.util.Configuration.*;

public class DbOpenHelper
    extends SQLiteOpenHelper
{

    private static final int DATABASE_VERSION = 14;

    private static final String DATABASE_NAME = "minerstatus.db";

    public DbOpenHelper( Context context )
    {
        super( context, DATABASE_NAME, null, DATABASE_VERSION );
    }

    @Override
    public void onCreate( SQLiteDatabase db )
    {
        db.execSQL( "CREATE TABLE miners (miner TEXT PRIMARY KEY, pool TEXT, errors INTEGER)" );
        db.execSQL( "CREATE TABLE config (key TEXT PRIMARY KEY, value TEXT)" );
        db.execSQL( "INSERT INTO config (key, value) VALUES ('" + KEY_THEME + "', 'dark')" );
        db.execSQL( "INSERT INTO config (key, value) VALUES ('" + KEY_SHOW_EXCHANGE_MTGOX + "', 'true')" );
        db.execSQL( "INSERT INTO config (key, value) VALUES ('" + KEY_CONNECTION_TIMEOUT + "', '4000')" );
        db.execSQL( "INSERT INTO config (key, value) VALUES ('" + KEY_MAXIMUM_ERRORS + "', '10')" );
        db.execSQL( "INSERT INTO config (key, value) VALUES ('" + KEY_WIDGET_APIKEY + "', 'none')" );
        db.execSQL( "INSERT INTO config (key, value) VALUES ('" + KEY_LOW_HASHRATE_NOTIFICATION + "', 'off')" );
        db.execSQL( "INSERT INTO config (key, value) VALUES ('" + KEY_VIBRATE_ON_NOTIFICATION + "', 'false')" );
        db.execSQL( "INSERT INTO config (key, value) VALUES ('" + KEY_SHOW_ADVERTISEMENTS + "', 'true')" );

        db.execSQL(
            "CREATE TABLE miner_data (miner TEXT, date_long INTEGER, json TEXT, pool_index INTEGER DEFAULT 0)" );
    }

    /**
     * Series of upgrade steps to move from one version to another.
     *
     * Version 14 is now considered the first version of the current published application at time of republishing
     * under com.trumpetx.minerstatus (formerly me.davidgreene.minerstatus)
     *
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade( SQLiteDatabase db, int oldVersion, int newVersion )
    {
        if ( oldVersion == 14 )
        {
            // Add your update code here!
            // Don't forget to make the static final version above match the oldVersion assignment on the next line.
            // oldVersion = 15;
        }

    }
}