package com.trumpetx.minerstatus.util;

import static com.trumpetx.minerstatus.util.Metadata.SOCKET_TIMEOUT;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.util.Log;

public class DataAgent
{
    final static String tag = "DA";

    private Configuration _configuration;

    public DataAgent( Configuration configuration )
    {
        _configuration = configuration;

        trustAllHosts();

        _connectionTimeout = _configuration.getConnectionTimeoutMilliseconds();

        HttpParams httpParameters = new BasicHttpParams();

        HttpConnectionParams.setConnectionTimeout( httpParameters, _connectionTimeout );
        HttpConnectionParams.setSoTimeout( httpParameters, SOCKET_TIMEOUT );

        _httpClient = new DefaultHttpClient( httpParameters );
    }

    private HttpClient _httpClient;

    private int _connectionTimeout;

    public void shutdown()
    {
        _httpClient.getConnectionManager().shutdown();
    }

    public String fetchData( String urlString )
        throws MalformedURLException
    {
        return fetchData( new URL( urlString ) );
    }

    public String fetchData( URL url )
    {
        final int MAXIMUM_ATTEMPTS = 3;

        for ( int attempt = 1; attempt <= MAXIMUM_ATTEMPTS; attempt++ )
        {
            boolean ignoreResponseCode = false;

            if ( attempt == MAXIMUM_ATTEMPTS )
                ignoreResponseCode = true;

            try
            {
                Log.d( tag, "Beginning attempt #" + attempt + ", URL: " + url );

                String data;

                String protocol = url.getProtocol();

                if ( protocol.equals( "http" ) )
                    data = fetchHttpData( url, ignoreResponseCode );
                else if ( protocol.equals( "https" ) )
                    data = fetchHttpsData( url, ignoreResponseCode );
                else if ( protocol.equals( "file" ) )
                    data = fetchFileData( url, ignoreResponseCode );
                else
                    throw new ProtocolException( "Unknown protocol: " + protocol );

                if ( data.contains( "invalid" ) || data.contains( "etcpasswd" ) )
                    data = "";

                return data;
            }
            catch ( Exception e )
            {
                Log.d( tag, "ERROR [" + url + "]: " + e );
            }
        }

        return "";
    }

    private String fetchHttpData( URL url, boolean ignoreResponseCode )
        throws IOException, ClientProtocolException, URISyntaxException
    {
        HttpGet request = new HttpGet( url.toURI() );

        ResponseHandler<String> handler = new BasicResponseHandler();

        return _httpClient.execute( request, handler );
    }

    private String fetchHttpsData( URL url, boolean ignoreResponseCode )
        throws IOException
    {
        HttpsURLConnection https = null;
        InputStream is = null;

        try
        {
            https = (HttpsURLConnection) url.openConnection();

            https.setHostnameVerifier( DO_NOT_VERIFY );
            https.setConnectTimeout( _connectionTimeout );
            https.connect();

            int responseCode = https.getResponseCode();
            String responseMessage = https.getResponseMessage();

            Log.d( tag, "HTTPS response: (" + responseCode + ") " + responseMessage );

            if ( ( responseCode < 0 ) && !ignoreResponseCode )
                throw new IOException( "Received HTTP response code " + responseCode + ", \"" + responseMessage
                    + "\", on URL: " + url );

            is = https.getInputStream();

            return readInputStream( is );
        }
        finally
        {
            if ( is != null )
                is.close();
            if ( https != null )
                https.disconnect();
        }
    }

    private String fetchFileData( URL url, boolean ignoreResponseCode )
        throws IOException, URISyntaxException
    {
        FileInputStream is = null;

        try
        {
            is = new FileInputStream( new File( url.toURI() ) );

            return readInputStream( is );
        }
        finally
        {
            if ( is != null )
                is.close();
        }
    }

    private String readInputStream( InputStream is )
        throws IOException
    {
        BufferedReader r = new BufferedReader( new InputStreamReader( is ), 1024 );
        StringBuilder streamData = new StringBuilder();
        String line;

        while ( ( line = r.readLine() ) != null )
            streamData.append( line );

        return streamData.toString();
    }

    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier()
    {
        @Override
        public boolean verify( String hostname, SSLSession session )
        {
            return true;
        }
    };

    /**
     * Trust every server - don't check for any certificate
     */
    private boolean _haveITrustedHostsYet = false;

    private void trustAllHosts()
    {
        if ( _haveITrustedHostsYet )
            return;

        _haveITrustedHostsYet = true;

        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager()
        {
            public java.security.cert.X509Certificate[] getAcceptedIssuers()
            {
                return new java.security.cert.X509Certificate[] {};
            }

            public void checkClientTrusted( X509Certificate[] chain, String authType )
                throws CertificateException
            {
            }

            public void checkServerTrusted( X509Certificate[] chain, String authType )
                throws CertificateException
            {
            }
        } };

        // Install the all-trusting trust manager
        try
        {
            SSLContext sc = SSLContext.getInstance( "TLS" );
            sc.init( null, trustAllCerts, new java.security.SecureRandom() );
            HttpsURLConnection.setDefaultSSLSocketFactory( sc.getSocketFactory() );
        }
        catch ( Exception e )
        {
            Log.d( tag, e.toString() );
        }
    }
}
