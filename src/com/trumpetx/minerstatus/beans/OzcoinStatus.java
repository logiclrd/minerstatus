package com.trumpetx.minerstatus.beans;

import android.widget.TableLayout;
import com.trumpetx.minerstatus.R;
import com.trumpetx.minerstatus.ViewMinerActivity;
import com.trumpetx.minerstatus.util.Configuration;
import com.trumpetx.minerstatus.util.Deserializer;
import com.trumpetx.minerstatus.util.GsonDeserializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OzcoinStatus
    extends StatusBase
    implements Serializable, Renderable
{

    /**
     *
     */
    private static final long serialVersionUID = -6915687432225003232L;

    public StatusMetadata getMetadata()
    {
        return new OzcoinStatusMetadata();
    }

    static class OzcoinStatusMetadata
        extends StatusMetadataBase
    {
        @Override
        public String getName()
        {
            return Configuration.POOL_OZCOIN;
        }

        @Override
        public String getLabel()
        {
            return "Ozcoin";
        }

        @Override
        public String getDirections()
        {
            String youCanGetYourAPIKey = "on your account details page at\nhttp://ozcoin.net/content/api-key";
            
            return getCommonDirections( "Ozco.in", youCanGetYourAPIKey );
        }

        @Override
        public boolean validateAPIKey( String apiKey )
        {
            return super.validateAPIKey( apiKey, DIGITS + LETTERS + "_" );
        }
        
        @Override
        public StatusDataAdapter getDataAdapter()
        {
            return new OzcoinStatusDataAdapter();
        }

        @Override
        public Deserializer<? extends Status> getDeserializer()
        {
            return new OzcoinStatusDeserializer();
        }
    }
    
    static class OzcoinStatusDataAdapter
        extends DefaultStatusDataAdapter
    {
        @Override
        public String[] getURLTemplates()
        {
            return new String[]{ "http://ozco.in/api.php?api_key=%MINER%" };
        }
    }
    
    static class OzcoinStatusDeserializer
        extends GsonDeserializer<OzcoinStatus>
    {
        public OzcoinStatusDeserializer()
        {
            super(OzcoinStatus.class);
        }
    }

    private OzcoinUser user;

    private Map<String, OzcoinWorker> worker = new HashMap<String, OzcoinWorker>();

    private String apiKey;

    @Override
    public void collectHashrates( List<BigDecimal> set )
    {
        set.add( getTotalHashrate() );

        for ( Map.Entry<String, OzcoinWorker> entry : getWorker().entrySet() )
        {
            OzcoinWorker workerStatus = entry.getValue();

            set.add( workerStatus.getCurrent_speed().multiply( ONE_MILLION ) );
        }
    }

    public void render( ViewMinerActivity activity )
    {
        TableLayout tl = (TableLayout) activity.findViewById( R.id.detailedView );
        tl.addView( activity.renderRow( "Hashrate:", super.formatHashrate( getTotalHashrate() ) ) );
        tl.addView( activity.renderRow( "Completed Payout (BTC):", getUser().getCompleted_payout() ) );
        tl.addView( activity.renderRow( "Estimated Payout (BTC):", getUser().getEstimated_payout() ) );
        tl.addView( activity.renderRow( "Completed Payout (NMC):", getUser().getCompleted_payout_namecoin() ) );
        tl.addView( activity.renderRow( "Estimated Payout (NMC):", getUser().getEstimated_payout_namecoin() ) );
        tl.addView( activity.renderRow( "PPS Fee:", getUser().getPps_fee() ) );
        tl.addView( activity.renderRow( "PPS Value:", getUser().getPps_value() ) );
        tl.addView( activity.renderRow( DEFAULT_USERNAME + ":", "" ) );
        if ( getWorker() != null )
        {
            for ( Map.Entry<String, OzcoinWorker> entry : getWorker().entrySet() )
            {
                OzcoinWorker workerStatus = entry.getValue();
                tl.addView( activity.renderRow( "", workerStatus.getUsername() ) );
                if ( workerStatus.getIdle_since() != "" )
                {
                    tl.addView( activity.renderRow( "Idle Since:", workerStatus.getIdle_since() ) );
                }
                tl.addView( activity.renderRow( "Hashrate:", super.formatHashrate(
                    workerStatus.getCurrent_speed().multiply( ONE_MILLION ) ) ) );
                if ( !workerStatus.getValid_shares().equals( BigDecimal.ZERO )
                    || !workerStatus.getInvalid_shares().equals( BigInteger.ZERO ) )
                {
                    tl.addView( activity.renderRow( "Efficiency: ", workerStatus.getEfficiency() + " %" ) );
                }
                tl.addView( activity.renderRow( "Valid Shares:", workerStatus.getValid_shares() ) );
                tl.addView( activity.renderRow( "Invalid Shares:", workerStatus.getInvalid_shares() ) );
                tl.addView( activity.renderRow( "Total Valid Shares:", workerStatus.getValid_shares_lifetime() ) );
                tl.addView( activity.renderRow( "Total Invalid Shares", workerStatus.getInvalid_shares_lifetime() ) );
                tl.addView( activity.renderRow( "", "" ) );
            }
        }
        tl.addView( activity.renderRow( "", "" ) );
    }

    public OzcoinUser getUser()
    {
        return user;
    }

    public void setUser( OzcoinUser user )
    {
        this.user = user;
    }

    public Map<String, OzcoinWorker> getWorker()
    {
        return worker;
    }

    public void setWorker( Map<String, OzcoinWorker> worker )
    {
        this.worker = worker;
    }

    @Override
    public String getUsername()
    {
        return DEFAULT_USERNAME;
    }

    @Override
    public String getDisplayCol1()
    {
        return getUser() == null
            ? ""
            : getUser().getCompleted_payout().setScale( 2, BigDecimal.ROUND_HALF_UP ).toString();
    }

    @Override
    public String getDisplayCol2()
    {
        return super.formatHashrate( getTotalHashrate() );
    }

    @Override
    public String getUsernameLabel()
    {
        return "";
    }

    @Override
    public String getDisplayCol1Label()
    {
        return "Payout";
    }

    @Override
    public String getDisplayCol2Label()
    {
        return HASHRATE_DISPLAY_COL_2_LABEL;
    }

    private static final BigDecimal ONE_MILLION = new BigDecimal( "1000000" );

    @Override
    public BigDecimal getTotalHashrate()
    {
        return getUser() == null ? BigDecimal.ZERO : getUser().getHashrate_raw().multiply( ONE_MILLION );
    }

    @Override
    public void setApiKey( String apiKey )
    {
        this.apiKey = apiKey;
    }

    public String getApiKey()
    {
        return apiKey;
    }

}
