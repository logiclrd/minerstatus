package com.trumpetx.minerstatus.beans;

import com.trumpetx.minerstatus.util.Deserializer;

public interface PluginMetadata
{
    public String getName();
    
    public String getLabel();

    public Deserializer<? extends Plugin> getDeserializer();
}
