package com.trumpetx.minerstatus.beans;

import android.widget.TableLayout;
import com.trumpetx.minerstatus.R;
import com.trumpetx.minerstatus.ViewMinerActivity;
import com.trumpetx.minerstatus.util.Configuration;
import com.trumpetx.minerstatus.util.Deserializer;
import com.trumpetx.minerstatus.util.GsonDeserializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class DeepbitStatus
    extends StatusBase
    implements Serializable, Renderable
{

    /**
     *
     */
    private static final long serialVersionUID = 6449549746661969052L;

    public StatusMetadata getMetadata()
    {
        return new DeepbitStatusMetadata();
    }

    static class DeepbitStatusMetadata
        extends StatusMetadataBase
    {
        @Override
        public String getName()
        {
            return Configuration.POOL_DEEPBIT;
        }

        @Override
        public String getLabel()
        {
            return "Deepbit.net";
        }

        @Override
        public String getDirections()
        {
            String youCanGetYourAPIKey = "(and generate new ones) on the JSON settings page at\nhttp://deepbit.net/settings";
            
            return getCommonDirections( "Deepbit.net", youCanGetYourAPIKey );
        }

        @Override
        public boolean validateAPIKey( String apiKey )
        {
            return super.validateAPIKey( apiKey, DIGITS + LETTERS + "_" );
        }
        
        @Override
        public StatusDataAdapter getDataAdapter()
        {
            return new DeepbitStatusDataAdapter();
        }

        @Override
        public Deserializer<? extends Status> getDeserializer()
        {
            return new DeepbitStatusDeserializer();
        }
    }
    
    static class DeepbitStatusDataAdapter
        extends DefaultStatusDataAdapter
    {
        @Override
        public String[] getURLTemplates()
        {
            return new String[]{ "https://deepbit.net/api/%MINER%" };
        }
    }
    
    static class DeepbitStatusDeserializer
        extends GsonDeserializer<DeepbitStatus>
    {
        public DeepbitStatusDeserializer()
        {
            super(DeepbitStatus.class);
        }
    }

    private BigDecimal confirmed_reward;

    private BigDecimal hashrate;

    private Boolean ipa = Boolean.FALSE;

    private Map<String, DeepbitWorker> workers;

    private String apiKey;

    @Override
    public void collectHashrates( List<BigDecimal> set )
    {
        set.add( getHashrate() );
    }

    @Override
    public BigDecimal getTotalHashrate()
    {
        return getHashrate().setScale( 2, BigDecimal.ROUND_HALF_UP );
    }

    public BigDecimal getConfirmed_reward()
    {
        return ( confirmed_reward == null ) ? BigDecimal.ZERO : confirmed_reward;
    }

    public void setConfirmed_reward( BigDecimal confirmed_reward )
    {
        this.confirmed_reward = confirmed_reward;
    }

    public BigDecimal getHashrate()
    {
        return ( hashrate == null ) ? BigDecimal.ZERO : hashrate;
    }

    public void setHashrate( BigDecimal hashrate )
    {
        this.hashrate = hashrate;
    }

    public Boolean getIpa()
    {
        return ipa;
    }

    public void setIpa( Boolean ipa )
    {
        this.ipa = ipa;
    }

    @Override
    public String getUsername()
    {
        return DEFAULT_USERNAME;
    }

    @Override
    public String getDisplayCol2()
    {
        return super.formatHashrate( hashrate );
    }

    public String getDisplayCol1()
    {
        return ( confirmed_reward == null ) ? "0" : confirmed_reward.toString();
    }

    public String getApiKey()
    {
        return apiKey;
    }

    public void setApiKey( String apiKey )
    {
        this.apiKey = apiKey;
    }

    @Override
    public String getUsernameLabel()
    {
        return "";
    }

    @Override
    public String getDisplayCol2Label()
    {
        return HASHRATE_DISPLAY_COL_2_LABEL;
    }

    @Override
    public String getDisplayCol1Label()
    {
        return CONFIRMED_REWARD_COL_1_LABEL;
    }

    public void setWorkers( Map<String, DeepbitWorker> workers )
    {
        this.workers = workers;
    }

    public Map<String, DeepbitWorker> getWorkers()
    {
        return workers;
    }

    public void render( ViewMinerActivity activity )
    {
        TableLayout tl = (TableLayout) activity.findViewById( R.id.detailedView );
        tl.addView( activity.renderRow( "Hashrate", super.formatHashrate( getHashrate() ) ) );
        tl.addView( activity.renderRow( "Confirmed Reward", getConfirmed_reward().toString() ) );
        tl.addView( activity.renderRow( "Ipa", getIpa().toString() ) );
        tl.addView( activity.renderRow( "Worker(s):", "" ) );
        if ( getWorkers() != null )
        {
            for ( String key : getWorkers().keySet() )
            {
                DeepbitWorker workerStatus = getWorkers().get( key );
                tl.addView( activity.renderRow( "", key ) );
                tl.addView( activity.renderRow( "Alive", workerStatus.getAlive().toString() ) );
                tl.addView( activity.renderRow( "Shares", workerStatus.getShares().toString() ) );
                tl.addView( activity.renderRow( "Stales", workerStatus.getStales().toString() ) );
                tl.addView( activity.renderRow( "", "" ) );
            }
        }
        tl.addView( activity.renderRow( "", "" ) );
    }

}
