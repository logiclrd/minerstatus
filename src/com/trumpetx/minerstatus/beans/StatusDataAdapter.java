package com.trumpetx.minerstatus.beans;

import com.trumpetx.minerstatus.util.DataAgent;

public interface StatusDataAdapter
{
    String[] getURLTemplates();

    String[] getURLs( String apiKey );

    String[] fetchData( String apiKey, DataAgent agent );
}
