package com.trumpetx.minerstatus.beans;

import android.widget.TableLayout;
import com.trumpetx.minerstatus.R;
import com.trumpetx.minerstatus.ViewMinerActivity;
import com.trumpetx.minerstatus.util.Configuration;
import com.trumpetx.minerstatus.util.Deserializer;
import com.trumpetx.minerstatus.util.GsonDeserializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class EclipseMcStatus
    extends StatusBase
    implements Serializable, Renderable
{
    /* @formatter:off */
    /**
{
  "apikey": "f430659bb703a1bac6839490b0e06a",
  "data":
  {
    "user":
    {
      "confirmed_rewards": "0.00809064",
      "unconfirmed_rewards": "0.00000000",
      "estimated_rewards": 0,
      "total_payout": "0.00000000",
      "blocks_found": "0"
    }
  },
  "workers":
  [
    {
      "worker_name": "trumpetx_worker",
      "hash_rate": " ",
      "round_shares": "0",
      "reset_shares": "256",
      "total_shares": "256",
      "last_activity": "2012-05-14 00:04:29"
    }
  ]
}
     */
    /* @formatter:on */
    private static final long serialVersionUID = 3330095142253113379L;

    public StatusMetadata getMetadata()
    {
        return new EclipseMcStatusMetadata();
    }

    static class EclipseMcStatusMetadata
        extends StatusMetadataBase
    {
        @Override
        public String getName()
        {
            return Configuration.POOL_ECLIPSEMC;
        }

        @Override
        public String getLabel()
        {
            return "EclipseMC";
        }

        @Override
        public String getDirections()
        {
            String youCanGetYourAPIKey = "on your account details page at\nhttps://eclipsemc.com/my_account.php";

            return getCommonDirections( "EclipseMC", youCanGetYourAPIKey );
        }

        @Override
        public StatusDataAdapter getDataAdapter()
        {
            return new EclipseMcStatusDataAdapter();
        }

        @Override
        public Deserializer<? extends Status> getDeserializer()
        {
            return new EclipseMcStatusDeserializer();
        }
    }

    static class EclipseMcStatusDataAdapter
        extends DefaultStatusDataAdapter
    {
        @Override
        public String[] getURLTemplates()
        {
            return new String[] { "https://eclipsemc.com/api.php?key=%MINER%&action=userstats" };
        }
    }
    
    static class EclipseMcStatusDeserializer
        extends GsonDeserializer<EclipseMcStatus>
    {
        public EclipseMcStatusDeserializer()
        {
            super(EclipseMcStatus.class);
        }
    }

    private String apikey;

    private EclipseMcData data;

    private EclipseMcWorker[] workers;

    @Override
    public void collectHashrates( List<BigDecimal> set )
    {
        set.add( getTotalHashrate() );

        for ( EclipseMcWorker worker : getWorkers() )
        {
            set.add( super.parseHashrate( worker.getHash_rate() ) );
        }
    }

    @Override
    public String getUsername()
    {
        return DEFAULT_USERNAME;
    }

    @Override
    public String getDisplayCol1()
    {
        EclipseMcUser user = getDataUser();

        return ( user == null ) ? "--" : user.getTotal_payout();
    }

    @Override
    public String getDisplayCol2()
    {
        return super.formatHashrate( getTotalHashrate() );
    }

    @Override
    public String getUsernameLabel()
    {
        return "";
    }

    @Override
    public String getDisplayCol1Label()
    {
        return "Total Payout";
    }

    @Override
    public String getDisplayCol2Label()
    {
        return HASHRATE_DISPLAY_COL_2_LABEL;
    }

    @Override
    public BigDecimal getTotalHashrate()
    {
        BigDecimal totalHashrate = BigDecimal.ZERO;

        for ( EclipseMcWorker worker : workers )
        {
            try
            {
                totalHashrate = totalHashrate.add( super.parseHashrate( worker.getHash_rate() ) );
            }
            catch ( Exception e )
            {
            }
        }

        return totalHashrate.setScale( 2, BigDecimal.ROUND_HALF_UP );
    }

    public String getApiKey()
    {
        return apikey;
    }

    public void setApiKey( String apiKey )
    {
        this.apikey = apiKey;
    }

    public EclipseMcData getData()
    {
        return data;
    }

    public void setData( EclipseMcData data )
    {
        this.data = data;
    }

    public EclipseMcUser getDataUser()
    {
        EclipseMcData data = getData();

        if ( data != null )
            return data.getUser();
        else
            return null;
    }

    public EclipseMcWorker[] getWorkers()
    {
        return workers;
    }

    public void setWorkers( EclipseMcWorker[] workers )
    {
        this.workers = workers;
    }

    public void render( ViewMinerActivity activity )
    {
        TableLayout tl = (TableLayout) activity.findViewById( R.id.detailedView );

        EclipseMcUser user = getDataUser();
        EclipseMcWorker[] workers = getWorkers();

        if ( user != null )
        {
            tl.addView( activity.renderRow( "Confirmed Rewards", user.getConfirmed_rewards() ) );
            tl.addView( activity.renderRow( "Unconfirmed Rewards", user.getUnconfirmed_rewards() ) );
            tl.addView( activity.renderRow( "Estimated Rewards", user.getEstimated_rewards().toString() ) );
            tl.addView( activity.renderRow( "Total Payout", user.getTotal_payout() ) );
            tl.addView( activity.renderRow( "Blocks Found", user.getBlocks_found() ) );
        }

        if ( workers != null )
        {
            for ( EclipseMcWorker worker : workers )
            {
                if ( worker != null )
                {
                    tl.addView( activity.renderRow( "", worker.getWorker_name() ) );
                    tl.addView( activity.renderRow( "Hashrate", super.formatHashrate( worker.getHash_rate() ) ) );
                    tl.addView( activity.renderRow( "Round Shares", worker.getRound_shares() ) );
                    tl.addView( activity.renderRow( "Reset Shares", worker.getReset_shares() ) );
                    tl.addView( activity.renderRow( "Total Shares", worker.getTotal_shares() ) );
                    tl.addView( activity.renderRow( "Last Activity", worker.getLast_activity() ) );

                    tl.addView( activity.renderRow( "", "" ) );
                }
            }
        }

        tl.addView( activity.renderRow( "", "" ) );
    }

}
