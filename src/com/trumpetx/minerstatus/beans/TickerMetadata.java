package com.trumpetx.minerstatus.beans;

import com.trumpetx.minerstatus.util.Deserializer;

public interface TickerMetadata
    extends PluginMetadata
{
     public TickerDataAdapter getDataAdapter();

     public Deserializer<? extends Ticker> getDeserializer();
}
