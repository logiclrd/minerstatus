package com.trumpetx.minerstatus.beans;

import com.trumpetx.minerstatus.util.DataAgent;

public abstract class DefaultStatusDataAdapter
    implements StatusDataAdapter
{
    public String[] getURLs( String apiKey )
    {
        String[] urlTemplates = getURLTemplates();

        for ( int i = 0; i < urlTemplates.length; i++ )
            urlTemplates[i] = urlTemplates[i].replace( "%MINER%", apiKey );

        return urlTemplates;
    }

    public String[] fetchData( String apiKey, DataAgent agent )
    {
        String[] urls = getURLs( apiKey );
        String[] data = new String[urls.length];

        for ( int i = 0; i < urls.length; i++ )
        {
            try
            {
                data[i] = agent.fetchData( urls[i] );
            }
            catch ( Throwable t )
            {
                data[i] = "invalid: " + t;
            }
        }

        return data;
    }
}
