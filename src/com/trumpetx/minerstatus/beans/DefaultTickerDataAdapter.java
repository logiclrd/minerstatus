package com.trumpetx.minerstatus.beans;

import com.trumpetx.minerstatus.util.DataAgent;

public abstract class DefaultTickerDataAdapter
    implements TickerDataAdapter
{
    @Override
    public String fetchData( DataAgent agent )
    {
        try
        {
            return agent.fetchData( getURL() );
        }
        catch ( Throwable t )
        {
            return "";
        }
    }
}
