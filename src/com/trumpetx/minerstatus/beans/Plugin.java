package com.trumpetx.minerstatus.beans;

public interface Plugin
{
    public PluginMetadata getMetadata();
}
