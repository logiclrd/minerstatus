package com.trumpetx.minerstatus.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import android.widget.TableLayout;

import com.trumpetx.minerstatus.R;
import com.trumpetx.minerstatus.ViewMinerActivity;
import com.trumpetx.minerstatus.util.Deserializer;

public class ItzodStatus
    extends StatusBase
    implements Serializable, Renderable
{
    /* @formatter:off */
    /*
[
  {
    "username":"Restor_0",
    "shares_total":"0",
    "shares_accepted":"0",
    "shares_diff1_approx":"0",
    "shares_diff1":"0",
    "shares_diff2":"0",
    "shares_diff4":"0",
    "shares_diff8":"0",
    "shares_diff16":"0",
    "shares_diff32":"0",
    "shares_total_24h":"0",
    "shares_accepted_24h":"0",
    "last_share":"1327908844",
    "w_speed":"0",
    "comment":"\u0437\u0430\u043b 1"
  },
  {
    "username":"Restor_2",
    "shares_total":"0",
    "shares_accepted":"0",
    "shares_diff1_approx":"0",
    "shares_diff1":"0",
    "shares_diff2":"0",
    "shares_diff4":"0",
    "shares_diff8":"0",
    "shares_diff16":"0",
    "shares_diff32":"0",
    "shares_total_24h":"0",
    "shares_accepted_24h":"0",
    "last_share":"1335346344",
    "w_speed":"0",
    "comment":"\u0437\u0430\u043b 2"
  },
  {
    "username":"Restor_3",
    "shares_total":"0",
    "shares_accepted":"0",
    "shares_diff1_approx":"0",
    "shares_diff1":"0",
    "shares_diff2":"0",
    "shares_diff4":"0",
    "shares_diff8":"0",
    "shares_diff16":"0",
    "shares_diff32":"0",
    "shares_total_24h":"0",
    "shares_accepted_24h":"0",
    "last_share":"1335346344",
    "w_speed":"0",
    "comment":"\u0437\u0430\u043b 3"
  },
  {
    "username":"Restor_4",
    "shares_total":"0",
    "shares_accepted":"0",
    "shares_diff1_approx":"0",
    "shares_diff1":"0",
    "shares_diff2":"0",
    "shares_diff4":"0",
    "shares_diff8":"0",
    "shares_diff16":"0",
    "shares_diff32":"0",
    "shares_total_24h":"0",
    "shares_accepted_24h":"0",
    "last_share":"1335346344",
    "w_speed":"0",
    "comment":"\u0437\u0430\u043b 4"
  },
  {
    "username":"Restor_5",
    "shares_total":"0",
    "shares_accepted":"0",
    "shares_diff1_approx":"0",
    "shares_diff1":"0",
    "shares_diff2":"0",
    "shares_diff4":"0",
    "shares_diff8":"0",
    "shares_diff16":"0",
    "shares_diff32":"0",
    "shares_total_24h":"0",
    "shares_accepted_24h":"0",
    "last_share":"1320872456",
    "w_speed":"0",
    "comment":"\u0441\u043f\u0430\u043b\u044c\u043d\u044f 1"
  },
  {
    "username":"Restor_6",
    "shares_total":"0",
    "shares_accepted":"0","
    shares_diff1_approx":"0",
    "shares_diff1":"0",
    "shares_diff2":"0",
    "shares_diff4":"0",
    "shares_diff8":"0",
    "shares_diff16":"0","
    shares_diff32":"0",
    "shares_total_24h":"0",
    "shares_accepted_24h":"0",
    "last_share":"1320872456",
    "w_speed":"0",
    "comment":"\u0441\u043f\u0430\u043b\u044c\u043d\u044f 2"
  }
]
     */
    /* @formatter:on */

    /**
     * 
     */
    private static final long serialVersionUID = -3077280921993927232L;

    @Override
    public StatusMetadata getMetadata()
    {
        return new ItzodStatusMetadata();
    }

    static class ItzodStatusMetadata
        extends StatusMetadataBase
    {
        @Override
        public String getName()
        {
            return "itzod";
        }

        @Override
        public String getLabel()
        {
            return "itzod";
        }

        @Override
        public String getDirections()
        {
            return "Your miner's name is the username you created when you opened an account with\nhttps://pool.itzod.ru";
        }

        @Override
        public String getAPIKeyLabel()
        {
            return "Mining Username";
        }

        @Override
        public StatusDataAdapter getDataAdapter()
        {
            return new ItzodStatusDataAdapter();
        }

        @Override
        public Deserializer<? extends Status> getDeserializer()
        {
            return new ItzodStatusDeserializer();
        }
    }

    static class ItzodStatusDataAdapter
        extends DefaultStatusDataAdapter
    {
        @Override
        public String[] getURLTemplates()
        {
            return new String[] { "https://pool.itzod.ru/apiex.php?act=getworkerstats&username=%MINER%" };
        }
    }

    private String apiKey;

    private ItzodUser[] users;

    public ItzodUser[] getUsers()
    {
        return users;
    }

    public void setUsers( ItzodUser[] users )
    {
        this.users = users;
    }

    @Override
    public void collectHashrates( List<BigDecimal> set )
    {
        set.add( getTotalHashrate() );

        ItzodUser[] users = getUsers();

        if ( users != null )
        {
            for ( ItzodUser user : users )
                set.add( user.getHashrate() );
        }
    }

    @Override
    public String getUsername()
    {
        return DEFAULT_USERNAME;
    }

    @Override
    public String getDisplayCol1()
    {
        return String.valueOf( getTotalShares() );
    }

    @Override
    public String getDisplayCol2()
    {
        return formatHashrate( getTotalHashrate() );
    }

    @Override
    public String getUsernameLabel()
    {
        return "";
    }

    @Override
    public String getDisplayCol1Label()
    {
        return "Shares (24 hours)";
    }

    @Override
    public String getDisplayCol2Label()
    {
        return HASHRATE_DISPLAY_COL_2_LABEL;
    }

    public int getTotalShares()
    {
        int sum = 0;

        ItzodUser[] users = getUsers();

        if ( users != null )
        {
            for ( ItzodUser user : users )
                sum += user.getShares_accepted_24h();
        }

        return sum;
    }

    @Override
    public BigDecimal getTotalHashrate()
    {
        BigDecimal sum = BigDecimal.ZERO;

        ItzodUser[] users = getUsers();

        if ( users != null )
        {
            for ( ItzodUser user : users )
                sum = sum.add( user.getHashrate() );
        }

        return sum;
    }

    @Override
    public void setApiKey( String apiKey )
    {
        this.apiKey = apiKey;
    }

    @Override
    public String getApiKey()
    {
        return apiKey;
    }

    @Override
    public void render( ViewMinerActivity activity )
    {
        TableLayout tl = (TableLayout) activity.findViewById( R.id.detailedView );

        ItzodUser[] users = getUsers();

        if ( users == null )
        {
            tl.addView( activity.renderRow( "", "No data." ) );
        }
        else
        {
            int totalWorkers = 0;
            ItzodUser totals = new ItzodUser();

            totals.setUsername( "Totals" );

            for ( ItzodUser user : users )
            {
                if ( user != null )
                {
                    totalWorkers++;

                    totals.add( user );

                    renderUser( tl, activity, user );
                }
            }

            if ( totalWorkers == 0 )
                tl.addView( activity.renderRow( "", "No workers." ) );
            else
                renderUser( tl, activity, totals );
        }
    }

    private void renderUser( TableLayout tl, ViewMinerActivity activity, ItzodUser user )
    {
        tl.addView( activity.renderRow( "", user.getUsername() ) );

        renderRowIfPresent( tl, activity, "Shares (total):", user.getShares_total() );
        renderRowIfPresent( tl, activity, "Shares (accepted):", user.getShares_accepted() );
        renderRowIfPresent( tl, activity, "Shares (diff 1 ~):", user.getShares_diff1_approx() );
        renderRowIfPresent( tl, activity, "Shares (diff 1):", user.getShares_diff1() );
        renderRowIfPresent( tl, activity, "Shares (diff 2):", user.getShares_diff2() );
        renderRowIfPresent( tl, activity, "Shares (diff 4):", user.getShares_diff4() );
        renderRowIfPresent( tl, activity, "Shares (diff 8):", user.getShares_diff8() );
        renderRowIfPresent( tl, activity, "Shares (diff 16):", user.getShares_diff16() );
        renderRowIfPresent( tl, activity, "Shares (diff 32):", user.getShares_diff32() );
        renderRowIfPresent( tl, activity, "Shares (24h total):", user.getShares_total_24h() );
        renderRowIfPresent( tl, activity, "Shares (24h accepted):", user.getShares_accepted_24h() );
        renderRowIfPresent( tl, activity, "Last Share:", user.getLast_share() );
        renderRowIfPresent( tl, activity, "Hashrate:", formatHashrate( user.getHashrate() ) );
        renderRowIfPresent( tl, activity, "Comment:", user.getComment() );

        tl.addView( activity.renderRow( "", "" ) );
    }

    private void renderRowIfPresent( TableLayout tl, ViewMinerActivity activity, String label, Object value )
    {
        if ( value != null )
            tl.addView( activity.renderRow( label, value ) );
    }
}
