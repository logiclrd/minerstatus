package com.trumpetx.minerstatus.beans;

import com.trumpetx.minerstatus.hashrateunits.HashrateUnit;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public interface Status
    extends Serializable, Plugin
{
    static final String DEFAULT_USERNAME = "Worker(s)";

    static final String CONFIRMED_REWARD_COL_1_LABEL = "Confirmed Reward";

    static final String HASHRATE_DISPLAY_COL_2_LABEL = "Hashrate";

    StatusMetadata getMetadata();
    
    Status mergeWith(Status other);

    void setHashrateFormat( boolean useComma, int decimalPlaces, HashrateUnit unit );

    String formatHashrate( BigDecimal hashrate );

    String formatHashrate( String hashrateString );

    void collectHashrates( List<BigDecimal> set );

    String getUsername();

    String getDisplayCol1();

    String getDisplayCol2();

    String getUsernameLabel();

    String getDisplayCol1Label();

    String getDisplayCol2Label();

    BigDecimal getTotalHashrate();

    void setApiKey( String apiKey );

    String getApiKey();
}
