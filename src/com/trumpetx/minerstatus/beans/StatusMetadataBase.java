package com.trumpetx.minerstatus.beans;

public abstract class StatusMetadataBase
    implements StatusMetadata
{
    protected String getCommonDirections( String poolName, String youCanGetYourAPIKey )
    {
        return
            poolName + " provides an API key which you can use to access your data semi-privately (security through obscurity.  " +
            "You can get your API key " + youCanGetYourAPIKey;
    }

    public String getAPIKeyLabel()
    {
        return "API Key";
    }
    
    protected final String DIGITS = "0123456789";
    protected final String UPPERCASE_LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    protected final String LOWERCASE_LETTERS = "abcdefghijklmnopqrstuvwxyz";
    
    protected final String LETTERS = UPPERCASE_LETTERS + LOWERCASE_LETTERS;
    
    protected boolean validateAPIKey( String apiKey, String validCharacters )
    {
        if ( apiKey.length() == 0 )
            return false;
        
        for (int i = 0; i < apiKey.length(); i++)
            if (validCharacters.indexOf( apiKey.charAt( i ) ) < 0)
                return false;
        
        return true;
    }

    public boolean validateAPIKey( String apiKey )
    {
        return validateAPIKey( apiKey, DIGITS + LETTERS );
    }
}
