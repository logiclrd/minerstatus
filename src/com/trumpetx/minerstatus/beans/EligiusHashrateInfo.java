package com.trumpetx.minerstatus.beans;

import java.io.Serializable;
import java.math.BigDecimal;

public class EligiusHashrateInfo
    implements Serializable
{
    private static final long serialVersionUID = 4752832235726493190L;

    private int interval;

    private String interval_name;

    private BigDecimal hashrate;

    private int shares;

    public int getInterval()
    {
        return interval;
    }

    public String getIntervalName()
    {
        return interval_name;
    }

    public BigDecimal getHashrate()
    {
        return hashrate;
    }

    public int getShares()
    {
        return shares;
    }
}
