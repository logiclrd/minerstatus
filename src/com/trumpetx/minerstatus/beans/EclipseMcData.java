package com.trumpetx.minerstatus.beans;

import java.io.Serializable;

public class EclipseMcData
    implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 6943103189446573973L;
    
    EclipseMcUser user;
    
    public EclipseMcUser getUser()
    {
        return user;
    }
    
    public void setUser(EclipseMcUser newValue)
    {
        user = newValue;
    }
}
