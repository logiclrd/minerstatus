package com.trumpetx.minerstatus.beans;

import com.trumpetx.minerstatus.util.DataAgent;

public interface TickerDataAdapter
{
    public String getURL();

    public String fetchData( DataAgent agent );
}
