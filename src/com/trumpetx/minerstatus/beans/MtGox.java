package com.trumpetx.minerstatus.beans;

import java.io.Serializable;

import com.trumpetx.minerstatus.util.Configuration;
import com.trumpetx.minerstatus.util.Deserializer;
import com.trumpetx.minerstatus.util.GsonDeserializer;

public class MtGox
    implements Serializable, Ticker
{

    /**
     * 
     */
    private static final long serialVersionUID = 5510944819713092766L;
    
    public TickerMetadata getMetadata()
    {
        return new MtGoxMetadata();
    }

    static class MtGoxMetadata extends TickerMetadataBase
    {
        @Override
        public String getName()
        {
            return Configuration.EXCHANGE_MTGOX;
        }

        @Override
        public String getLabel()
        {
            return "Mt. Gox";
        }

        @Override
        public TickerDataAdapter getDataAdapter()
        {
            return new MtGoxDataAdapter();
        }

        @Override
        public Deserializer<? extends Ticker> getDeserializer()
        {
            return new MtGoxDeserializer();
        }
    }
    
    static class MtGoxDataAdapter extends DefaultTickerDataAdapter
    {
        @Override
        public String getURL()
        {
            return "https://data.mtgox.com/api/2/BTCUSD/money/ticker";
        }
    }
    
    static class MtGoxDeserializer
        extends GsonDeserializer<MtGox>
    {
        public MtGoxDeserializer()
        {
            super(MtGox.class);
        }
    }

    private String result;

    private MtGoxData data;

    public MtGoxData getData()
    {
        return data;
    }

    public void setData( MtGoxData data )
    {
        this.data = data;
    }

    public String getResult()
    {
        return result;
    }

    public void setResult( String result )
    {
        this.result = result;
    }

    private String stripFluff( String str )
    {
        StringBuffer sb;
        sb = new StringBuffer( str.length() );
        for ( char c : str.toCharArray() )
        {
            if ( Character.isDigit( c ) || c == '.' )
            {
                sb.append( c );
            }
        }
        return sb.toString();
    }

    @Override
    public String getLastString()
    {
        try
        {
            return stripFluff( getData().getLast().getDisplay_short() );
        }
        catch ( Exception e )
        {
            return "";
        }
    }

    @Override
    public String getHighString()
    {
        try
        {
            return stripFluff( getData().getHigh().getDisplay_short() );
        }
        catch ( Exception e )
        {
            return "";
        }
    }

    @Override
    public String getLowString()
    {
        try
        {
            return stripFluff( getData().getLow().getDisplay_short() );
        }
        catch ( Exception e )
        {
            return "";
        }
    }

    @Override
    public String getBuyString()
    {
        try
        {
            return stripFluff( getData().getBuy().getDisplay_short() );
        }
        catch ( Exception e )
        {
            return "";
        }
    }

    @Override
    public String getSellString()
    {
        try
        {
            return stripFluff( getData().getSell().getDisplay_short() );
        }
        catch ( Exception e )
        {
            return "";
        }
    }

    @Override
    public String getVolString()
    {
        try
        {
            return stripFluff( getData().getVol().getDisplay_short() );
        }
        catch ( Exception e )
        {
            return "";
        }
    }
}
