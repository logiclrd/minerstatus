package com.trumpetx.minerstatus.beans;

import android.widget.TableLayout;
import com.trumpetx.minerstatus.R;
import com.trumpetx.minerstatus.ViewMinerActivity;
import com.trumpetx.minerstatus.util.Configuration;
import com.trumpetx.minerstatus.util.Deserializer;
import com.trumpetx.minerstatus.util.GsonDeserializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TripleminingStatus
    extends StatusBase
    implements Serializable, Renderable
{
    /* @formatter:off */
    /*
    {
      "confirmed_reward": "0.00000000",
      "hashrate": "0",
      "workers":
      {
        "trumpetx_nm":
        {
          "shares": "0",
          "alive": "false",
          "stales": "0"
        }
      },
      "total_shares": "9995142",
      "estimated_payout": "0.00000000"
    }
    */
    /* @formatter:on */

    private static final long serialVersionUID = 9105053716084690211L;

    public StatusMetadata getMetadata()
    {
        return new TripleminingStatusMetadata();
    }

    static class TripleminingStatusMetadata
        extends StatusMetadataBase
    {
        @Override
        public String getName()
        {
            return Configuration.POOL_TRIPLE;
        }

        @Override
        public String getLabel()
        {
            return "Triple Mining";
        }

        @Override
        public String getDirections()
        {
            String youCanGetYourAPIKey = "on your workers page at\nhttps://www.triplemining.com/workers";

            return getCommonDirections( "Triple Mining", youCanGetYourAPIKey );
        }

        @Override
        public StatusDataAdapter getDataAdapter()
        {
            return new TripleminingStatusDataAdapter();
        }
        
        @Override
        public Deserializer<? extends Status> getDeserializer()
        {
            return new TripleminingStatusDeserializer();
        }
    }

    static class TripleminingStatusDataAdapter
        extends DefaultStatusDataAdapter
    {
        @Override
        public String[] getURLTemplates()
        {
            return new String[] { "https://api.triplemining.com/json/%MINER%" };
        }
    }
    
    static class TripleminingStatusDeserializer
        extends GsonDeserializer<TripleminingStatus>
    {
        public TripleminingStatusDeserializer()
        {
            super(TripleminingStatus.class);
        }
    }

    private BigDecimal confirmed_reward;

    private BigDecimal hashrate;

    private Integer total_shares;

    private BigDecimal estimated_payout;

    private String apiKey;

    private Map<String, TripleMiningWorker> workers = new HashMap<String, TripleMiningWorker>();

    @Override
    public void collectHashrates( List<BigDecimal> set )
    {
        set.add( getHashrate() );
    }

    @Override
    public String getUsername()
    {
        return DEFAULT_USERNAME;
    }

    @Override
    public String getDisplayCol1()
    {
        return confirmed_reward.toString();
    }

    @Override
    public String getDisplayCol2()
    {
        return super.formatHashrate( hashrate );
    }

    @Override
    public String getUsernameLabel()
    {
        return "";
    }

    @Override
    public String getDisplayCol1Label()
    {
        return CONFIRMED_REWARD_COL_1_LABEL;
    }

    @Override
    public String getDisplayCol2Label()
    {
        return HASHRATE_DISPLAY_COL_2_LABEL;
    }

    public BigDecimal getConfirmed_reward()
    {
        return confirmed_reward;
    }

    public void setConfirmed_reward( BigDecimal confirmed_reward )
    {
        this.confirmed_reward = confirmed_reward;
    }

    public BigDecimal getHashrate()
    {
        return hashrate == null ? BigDecimal.ZERO : hashrate;
    }

    public void setHashrate( BigDecimal hashrate )
    {
        this.hashrate = hashrate;
    }

    public Integer getTotal_shares()
    {
        return total_shares == null ? 0 : total_shares;
    }

    public void setTotal_shares( Integer total_shares )
    {
        this.total_shares = total_shares;
    }

    public BigDecimal getEstimated_payout()
    {
        return estimated_payout == null ? BigDecimal.ZERO : estimated_payout;
    }

    public void setEstimated_payout( BigDecimal estimated_payout )
    {
        this.estimated_payout = estimated_payout;
    }

    public String getApiKey()
    {
        return apiKey;
    }

    public void setApiKey( String apiKey )
    {
        this.apiKey = apiKey;
    }

    @Override
    public BigDecimal getTotalHashrate()
    {
        return getHashrate();
    }

    public Map<String, TripleMiningWorker> getWorkers()
    {
        return workers;
    }

    public void setWorkers( Map<String, TripleMiningWorker> workers )
    {
        this.workers = workers;
    }

    public void render( ViewMinerActivity activity )
    {
        TableLayout tl = (TableLayout) activity.findViewById( R.id.detailedView );
        tl.addView( activity.renderRow( "Hashrate", super.formatHashrate( getHashrate() ) ) );
        tl.addView( activity.renderRow( "Confirmed Reward", getConfirmed_reward().toString() ) );
        tl.addView( activity.renderRow( "Estimated Payout", getEstimated_payout().toString() ) );
        tl.addView( activity.renderRow( "Total Shares", getTotal_shares().toString() ) );
        tl.addView( activity.renderRow( DEFAULT_USERNAME + ":", "" ) );
        if ( getWorkers() != null )
        {
            for ( String key : getWorkers().keySet() )
            {
                TripleMiningWorker workerStatus = getWorkers().get( key );
                tl.addView( activity.renderRow( "", key ) );
                tl.addView( activity.renderRow( "Alive", workerStatus.getAlive().toString() ) );
                tl.addView( activity.renderRow( "Shares", workerStatus.getShares().toString() ) );
                tl.addView( activity.renderRow( "Stales", workerStatus.getStales().toString() ) );
                tl.addView( activity.renderRow( "", "" ) );
            }
        }
        tl.addView( activity.renderRow( "", "" ) );
    }
}
