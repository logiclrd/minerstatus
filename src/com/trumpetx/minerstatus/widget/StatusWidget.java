package com.trumpetx.minerstatus.widget;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;
import com.trumpetx.minerstatus.MainMinerActivity;
import com.trumpetx.minerstatus.MinerStatusApp;
import com.trumpetx.minerstatus.R;
import com.trumpetx.minerstatus.beans.Result;
import com.trumpetx.minerstatus.beans.Status;
import com.trumpetx.minerstatus.beans.StatusMetadata;
import com.trumpetx.minerstatus.hashrateunits.HashrateUnit;
import com.trumpetx.minerstatus.service.MinerService;
import com.trumpetx.minerstatus.tasks.AsynchMinerUpdateTask;
import com.trumpetx.minerstatus.util.Configuration;
import com.trumpetx.minerstatus.util.Deserializer;
import com.trumpetx.minerstatus.util.Metadata;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

public class StatusWidget
    extends AppWidgetProvider
{
    private static final String tag = "SW";

    @Override
    public void onUpdate( Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds )
    {
        context.startService( new Intent( context, UpdateService.class ) );
    }

    public static class UpdateService
        extends Service
    {
        private MinerService _minerService;

        private Configuration _configuration;

        private AsynchMinerUpdateTask _myAsynchMinerUpdateTask;

        @Override
        public int onStartCommand( Intent intent, int flags, int startId )
        {
            MinerStatusApp application = (MinerStatusApp) this.getApplicationContext();

            _minerService = application.getMinerService();
            _configuration = new Configuration( application );

            _myAsynchMinerUpdateTask =
                new MyAsynchMinerUpdateTask( _minerService, _configuration, _configuration.getWidgetApiKey() );

            String apiKey = _configuration.getWidgetApiKey();

            if ( !apiKey.equals( "" ) )
            {
                _myAsynchMinerUpdateTask.execute();
            }
            else
            {
                RemoteViews updateView = new RemoteViews( this.getPackageName(), R.layout.status_message );

                updateView.setTextViewText( R.id.pool, "Configure in MinerStatus" );
                updateView.setTextViewText( R.id.col1label, "No API Key set" );
                updateView.setTextViewText( R.id.col1, "" );
                updateView.setTextViewText( R.id.col2label, "" );
                updateView.setTextViewText( R.id.col2, "" );
                updateView.setTextViewText( R.id.updated, "" );

                AppWidgetManager.getInstance( this ).updateAppWidget( new ComponentName( this, StatusWidget.class ),
                                                                      updateView );
            }

            // TODO: Figure out how to have multiple widgets!

            return START_STICKY;
        }

        public void buildUpdate( Context context )
        {
            RemoteViews updateView = new RemoteViews( context.getPackageName(), R.layout.status_message );

            try
            {
                String apiKey = _configuration.getWidgetApiKey();

                if ( apiKey.equals( "" ) )
                {
                    throw new Exception( "No apiKey" );
                }

                String pool = _minerService.getPoolForMiner( apiKey );

                if ( pool == null || pool.equals( "" ) )
                {
                    throw new Exception( "No pool" );
                }

                StatusMetadata statusMetadata = Metadata.getPoolMetadata( pool );

                Deserializer<? extends Status> statusDeserializer = statusMetadata.getDeserializer();

                List<Result> minerResultList = _minerService.readJsonData( apiKey );

                Status status = null;

                for ( Result minerResult : minerResultList )
                {
                    Status deserialized = statusDeserializer.deserialize( minerResult.getData() );

                    if ( status == null )
                        status = deserialized;
                    else
                        status.mergeWith( deserialized );
                }

                status.setApiKey( apiKey );

                boolean hashrateFormatCommas = _configuration.getHashrateFormatCommas();
                int hashrateFormatDecimals = _configuration.getHashrateFormatDecimals();
                HashrateUnit hashrateUnit = _configuration.getHashrateUnit();

                if ( hashrateUnit.getIsAuto() )
                {
                    List<BigDecimal> collectedHashratesForAutomaticUnitSelection = new ArrayList<BigDecimal>();

                    status.collectHashrates( collectedHashratesForAutomaticUnitSelection );

                    hashrateUnit.autoSelectBestUnit( collectedHashratesForAutomaticUnitSelection );
                }

                status.setHashrateFormat( hashrateFormatCommas, hashrateFormatDecimals, hashrateUnit );

                updateView.setTextViewText( R.id.pool, statusMetadata.getLabel() );
                updateView.setTextViewText( R.id.col1label, status.getDisplayCol1Label() + ":" );
                updateView.setTextViewText( R.id.col1, status.getDisplayCol1() );
                updateView.setTextViewText( R.id.col2label, status.getDisplayCol2Label() + ":" );
                updateView.setTextViewText( R.id.col2, status.getDisplayCol2() );
                updateView.setTextViewText( R.id.updated,
                                            DateFormat.getTimeInstance( DateFormat.SHORT ).format( minerResultList.get( 0 ).getDate() ) );

                sendNotification( status );
            }
            catch ( Exception e )
            {
                Log.d( tag, e.toString() );
            }

            AppWidgetManager.getInstance( this ).updateAppWidget( new ComponentName( this, StatusWidget.class ),
                                                                  updateView );
        }

        private static final int MINER_NOTIFICATION_ID = 309392;

        @SuppressWarnings( "deprecation" )
        private void sendNotification( Status status )
        {
            BigDecimal lowHashrateNotification = _configuration.getLowHashrateNotification();

            if ( lowHashrateNotification != null )
            {
                if ( status.getTotalHashrate().compareTo( lowHashrateNotification ) <= 0 )
                {
                    NotificationManager mNotificationManager =
                        (NotificationManager) getSystemService( Context.NOTIFICATION_SERVICE );

                    int icon = R.drawable.icon;
                    CharSequence tickerText = "Hashrate: " + status.formatHashrate( status.getTotalHashrate() );
                    long when = System.currentTimeMillis();

                    Notification notification = new Notification( icon, tickerText, when );

                    Context context = getApplicationContext();
                    CharSequence contentTitle = "Hashrate Alert";
                    CharSequence contentText =
                        "Your Hashrate has dropped to: " + status.formatHashrate( status.getTotalHashrate() );
                    Intent notificationIntent = new Intent( this, MainMinerActivity.class );
                    PendingIntent contentIntent = PendingIntent.getActivity( this, 0, notificationIntent, 0 );

                    notification.defaults |= Notification.DEFAULT_LIGHTS;
                    notification.flags |= Notification.FLAG_AUTO_CANCEL;

                    notification.setLatestEventInfo( context, contentTitle, contentText, contentIntent );

                    boolean vibrate = _configuration.getVibrateOnNotification();

                    if ( vibrate )
                    {
                        notification.defaults |= Notification.DEFAULT_VIBRATE;
                    }

                    mNotificationManager.notify( MINER_NOTIFICATION_ID, notification );
                }
                else
                {
                    // good to go, reset notification count TODO
                }
            }
        }

        @Override
        public IBinder onBind( Intent intent )
        {
            return null;
        }

        private class MyAsynchMinerUpdateTask
            extends AsynchMinerUpdateTask
        {
            public MyAsynchMinerUpdateTask( MinerService minerService, Configuration configuration,
                                            String... apiKeysToUpdate )
            {
                super( minerService, configuration, apiKeysToUpdate );
            }

            @Override
            protected void onPostExecute( Boolean result )
            {
                buildUpdate( UpdateService.this );
            }
        }

        @Override
        public void onDestroy()
        {
            super.onDestroy();
            this._myAsynchMinerUpdateTask.cancel( true );
            this._myAsynchMinerUpdateTask = null;
        }
    }
}
