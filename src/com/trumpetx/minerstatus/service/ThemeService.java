package com.trumpetx.minerstatus.service;

import com.trumpetx.minerstatus.theme.Theme;

public interface ThemeService
{
    public Theme getTheme();
}
