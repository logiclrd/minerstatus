package com.trumpetx.minerstatus.service;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.trumpetx.minerstatus.MinerStatusApp;
import com.trumpetx.minerstatus.beans.Result;
import com.trumpetx.minerstatus.util.Metadata;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class MinerServiceImpl
    implements MinerService
{
    private final String tag = "MS_";

    private MinerStatusApp app;

    public MinerServiceImpl( Context context )
    {
        this.app = ( (MinerStatusApp) context );
    }

    public void updateErrorCount( String miner, int errorNum )
    {
        ContentValues args = new ContentValues();
        args.put( "errors", errorNum );
        getDBw().update( "miners", args, "miner=?", new String[]{ miner } );
    }

    public void deleteMiner( String miner )
    {
        getDBw().delete( "miners", "miner=?", new String[]{ miner } );
    }

    private SQLiteDatabase getDBw()
    {
        return app.getDbHelper().getWritableDatabase();
    }

    // NB: because there exists a method getPoolForMiner(String miner) and code that assumes it can use
    // it, a given API key can only be used with one pool. This evidently isn't a problem, because there
    // would be strange bugs if any users had been setting up multiple pools with the same API key before
    // this change, with data from one pool being cached for another and stuff. Nevertheless, it should be
    // enforced.
    //private final String SELECT_MINER = "SELECT miner FROM miners WHERE miner=? AND pool=?";
    private final String SELECT_MINER = "SELECT miner FROM miners WHERE miner=?";

    @Override
    public Boolean minerExists( String miner, String pool )
    {
        Cursor cursor = null;

        try
        {
            //cursor = getDBw().rawQuery(SELECT_MINER, new String[] { miner, pool });
            cursor = getDBw().rawQuery( SELECT_MINER, new String[]{ miner } );

            return cursor.moveToNext();
        }
        finally
        {
            if ( cursor != null && !cursor.isClosed() )
            {
                cursor.close();
            }
        }
    }

    @Override
    public void insertMiner( String miner, String pool )
    {
        ContentValues values = new ContentValues();
        values.put( "miner", miner );
        values.put( "pool", pool );
        values.put( "errors", Metadata.MAX_ERRORS - 1 ); // This will
        // be reset
        // to 0 if
        // it is
        // successful
        getDBw().insert( "miners", null, values );
    }

    public void addJsonData( String miner, String jsonData, Integer poolIndex )
    {
        ContentValues values = new ContentValues();
        values.put( "miner", miner );
        values.put( "json", jsonData );
        values.put( "pool_index", poolIndex );
        values.put( "date_long", System.currentTimeMillis() );
        getDBw().insert( "miner_data", null, values );
    }

    // public final String GET_LATEST_MINER_DATA =
    // "SELECT json, date_long FROM miner_data WHERE date_long=(SELECT MAX(date_long) FROM miner_data WHERE miner=?) AND miner=?";
    // Select(max) columns need to be indexed, using an alternate method
    public final String GET_LATEST_MINER_DATA =
        "SELECT json, date_long, pool_index FROM miner_data WHERE miner=? ORDER BY date_long DESC";

    public final String CLEAR_DAY_OLD_DATA = "DELETE FROM miner_data WHERE miner=? and date_long < ?";

    public List<Result> readJsonData( String miner )
    {
        Cursor cursor = null;
        try
        {
            Long oneDayAgo = System.currentTimeMillis() - 86400000L;
            cursor = getDBw().rawQuery( CLEAR_DAY_OLD_DATA, new String[]{ miner, oneDayAgo.toString() } );

            cursor = getDBw().rawQuery( GET_LATEST_MINER_DATA, new String[]{ miner } );

            // Only select the first row since we're going for the
            // max(date_long)
            List<Result> jsonResultList = new LinkedList<Result>();
            Set<Integer> poolIndexSet = new HashSet<Integer>();
            while ( cursor.moveToNext() )
            {
                if ( poolIndexSet.contains( cursor.getInt( 2 ) ) )
                {
                    break;
                }
                poolIndexSet.add( cursor.getInt( 2 ) );
                Result result = new Result();
                result.setData( cursor.getString( 0 ) );
                result.setDate( new Date( cursor.getLong( 1 ) ) );
                jsonResultList.add( result );
            }
            return jsonResultList;
        }
        catch ( Exception e )
        {
            Log.d( tag, e.getMessage() );
        }
        finally
        {
            if ( cursor != null && !cursor.isClosed() )
            {
                cursor.close();
            }
        }
        return null;
    }

    private final String SELECT_POOLS = "SELECT distinct pool FROM miners order by pool asc";

    public Cursor getPools()
    {
        return getDBw().rawQuery( SELECT_POOLS, null );
    }

    private final String SELECT_MINERS_BY_POOL = "SELECT miner, errors FROM miners WHERE pool=?";

    public Cursor getMiners( String pool )
    {
        return getDBw().rawQuery( SELECT_MINERS_BY_POOL, new String[]{ pool } );
    }

    private final String SELECT_MINERS = "SELECT miner FROM miners";

    public Cursor getMiners()
    {
        return getDBw().rawQuery( SELECT_MINERS, new String[]{ } );
    }

    private final String SELECT_POOL = "SELECT DISTINCT pool FROM miners WHERE miner=?";

    public String getPoolForMiner( String miner )
    {
        Cursor cursor = null;
        try
        {
            cursor = getDBw().rawQuery( SELECT_POOL, new String[]{ miner } );
            if ( cursor.moveToNext() )
            {
                return cursor.getString( 0 );
            }
        }
        finally
        {
            if ( cursor != null && !cursor.isClosed() )
            {
                cursor.close();
            }
        }

        return "";
    }
}
