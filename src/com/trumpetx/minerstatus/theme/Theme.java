package com.trumpetx.minerstatus.theme;

public interface Theme
{

    public int getTextColor();

    public int getBackgroundColor();

    public int getHeaderTextColor();

}
